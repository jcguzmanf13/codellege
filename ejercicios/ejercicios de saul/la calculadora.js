Write a function called squareNumber that will take one argument (a number), square that number, and return the result.
It should also log a string like "The result of squaring the number 3 is 9."

function squareNumber(num){
var res = num * num
    console.log('la raiz cuadrada de ' + num + ' es ' + res);
    return res;
}

Write a function called halfNumber that will take one argument (a number), divide it by 2, and return the result.
It should also log a string like "Half of 5 is 2.5.".

function halfNumber(num){
var res = num / 2
console.log('la mitad de ' + num + ' es ' + res);
return res;
}

Write a function called percentOf that will take two numbers,
figure out what percent the first number represents of the second number, and return the result.
It should also log a string like "2 is 50% of 4."

function percentOf(num1,num2){
var res = (num1/num2)*100;
console.log(num1 + ' es ' + res + '% de ' + num2);
    return res;
}

Write a function called areaOfCircle that will take one argument (the radius),
calculate the area based on that, and return the result.
 It should also log a string like "The area for a circle with radius 2 is 12.566370614359172."

 function areaOfCircle(num){
 var res = 3.1416*(num*num)
 console.log('el area del circulo es ' +res)
 return res;
 }

 Write a function that will take one argument (a number) and perform the following operations, using the functions you wrote earlier1:
Take half of the number and store the result.
Square the result of #1 and store that result.
Calculate the area of a circle with the result of #2 as the radius.
Calculate what percentage that area is of the squared result (#3).

function todas(num){
var uno = num / 2;
var dos = uno * uno;
var tres = 3.1416*(dos*dos);
var cuatro = (dos,tres);
console.log('la mitad es ' +uno 'el cuadrado es ' +dos 'el radio es ' +tres 'el porcentaje es ' +cuatro);
}
