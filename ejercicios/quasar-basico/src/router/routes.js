
const routes = [
  {
    path: '/',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') }
    ]
  },

  {
    path: '/stepper',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', component: () => import('pages/stepper.vue') }
    ]
  },

  {
    path: '/data',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', component: () => import('pages/data.vue') }
    ]
  },

  {
    path: '/tabs',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', component: () => import('pages/tabs.vue') }
    ]
  },

  {
    path: '/stepper',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', component: () => import('pages/stepper.vue') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
